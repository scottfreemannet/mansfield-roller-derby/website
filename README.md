# Mansfield Roller Derby

Public website for Mansfield Roller Derby

## Getting Started

Ensure you are working in the `develop` branch only.

You need NPM and Gem installed.

Build the site using Jekyll (if you don't have Jekyll installed, see the tutorial or docs):
```
jekyll serve
```

Once your code is ready, commit and it'll start the pipeline to publish to preview.

Live rollout is manual at this time.