---
layout: blog-post
title: 'Hey Arnold!'
date: 2017-02-05
categories: skater
tags: skater beginner champs
thumbnail: 01.jpg
featured_image: 01.jpg
excerpt: 'Hey Arnold!, one of Mansfields newer skaters, talks of her experience joining the league and competing in her first British Championships game.'
---

*This is a guest post by Hey Arnold!, one of our newer skaters.*

My first session with MRD was in April 2017, I was so nervous as I hadn't been on skates in nearly 20 years and I wasn't great then. I had so much fun even though stopping was a problem.

With fantastic coaching, training, help and support of the lovely people I played my first scrim in Leicester on Wednesday 25th January. It was absolutely fantastic, the team really supported me even when I doubted myself. I had a great time and we won.

Today, Sunday 4th February I took part in the first 2017 tier 4 champs game . Again, I was nervous and wasn't sure I was good enough but I played my first jam, it was hard and I made some mistakes but knew that I could do better. I played more jams and I felt I got better with each one. Working and listening with and to the other blockers, bench and line manager.  It was fantastic, I didn't receive any penalties and we won.

It felt so good to be a part of something and play with people who don't judge and are more than happy to give you tips and tell you about their experiences and most important make you part of the team. I am so proud of myself,  the team I play with and the people that support the team.

I can't wait to build on the skills I have ready learnt, roller derby is a brilliant sport and is fun and interesting. I'm looking forward to what 2017 will bring.
