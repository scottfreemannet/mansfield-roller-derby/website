---
layout: blog-post
title: 'Ask Refresh: When am I out of bounds?'
date: 2017-02-11
categories: ask-refresh
tags: rules referee refresh
thumbnail: 01.jpg
excerpt: 'In this Ask Refresh, where skaters ask our resident referee Refresh questions, we find out when you are really out of bounds'
---

Recently, I was asked this question at training:

>If I am blocked, fall to the ground, and my arm touches outside the track boundary but the rest of my body stays on track, do I need to recycle myself?

The glossary definition of [out of bounds](https://rules.wftda.com/90_glossary.html#term-out-of-bounds) says: "A Skater is out of bounds when part of the Skater’s body or equipment is touching the ground beyond the track boundary, including both arms or hands (one arm or hand does not render a Skater out of bounds), or any part below the Skater’s waist (for example, a knee, a skate, or a hip)"

If you were knocked down and only one single arm touched beyond the track boundary, you are not considered out of bounds, and therefore do not meet the criteria for a cutting penalty.

Further, a skater is still considered upright if only one hand is touching the floor. As such, you could place a single hand outside the track boundary while passing opposing skaters and still be considered upright and inbounds. In this situation, you would not be assessed a cutting penalty.

However, if both hands touched beyond the track boundary at the same time, this would render the skater out of bounds and could meet the criteria for cutting. The skater would need to leave the track and recycle themselves behind the initiating blocker and all blockers in front of them at the moment they left track.

If you have a question on the rules that you would like clarifying, please get in touch!
