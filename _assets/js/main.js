//= require jquery-3.1.1.min.js
//= require bootstrap.min.js
//= require jquery.lazyload.min.js

$(function() {
    $('.pagination .active a').click(function() {
        return false;
    });
    if ($('.slideshow').length > 0) {
      function slideShow() {
        var $container = $('.slideshow'),
            $currentSlide = $container.find('.current'),
            $nextSlide = $currentSlide.next();
        if ($nextSlide.length === 0) {
          $nextSlide = $container.find('div').first();
        }
        $nextSlide.addClass('current');
        setTimeout(function() {
          $currentSlide.removeClass('current');
        }, 400)
      }
      var speed = $('.slideshow').data('speed') ? $('.slideshow').data('speed') : 7;
      var slideShowTimer = setInterval(slideShow, speed * 1000);
    };
    $('.flip-container').on('click touchend', function(e) {
      e.preventDefault();
      if ($(e.target).is('a')) {
        window.location = $(e.target).attr('href');
      } else {
        $(this).toggleClass('hover');
      }
    });
    $('.options').on('click', 'span', function(e) {
      e.preventDefault();
      $(this).parent().find('span').removeClass('selected');
      $(this).addClass('selected');
    });
    $("img.lazy").lazyload({
      effect : "fadeIn"
    });

    if ($('#cd-timeline').length > 0) {
    	var timelineBlocks = $('.cd-timeline-block'),
    		offset = 0.8;

    	hideBlocks(timelineBlocks, offset);
    	$(window).on('scroll', function(){
    		(!window.requestAnimationFrame)
    			? setTimeout(function(){ showBlocks(timelineBlocks, offset); }, 100) : window.requestAnimationFrame(function(){ showBlocks(timelineBlocks, offset); });
    	});

    	function hideBlocks(blocks, offset) {
    		blocks.each(function(){
    			( $(this).offset().top > $(window).scrollTop()+$(window).height()*offset ) && $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
    		});
    	}

    	function showBlocks(blocks, offset) {
    		blocks.each(function(){
    			( $(this).offset().top <= $(window).scrollTop()+$(window).height()*offset && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) && $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
    		});
    	}
    }
});
