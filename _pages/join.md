---
layout: default
title: Join Us
permalink: /join/
---

There are many ways to get involved with Mansfield Roller Derby. We welcome people from all backgrounds, and will be happy to have you join us!

Become a skater
---------------
If you're interested in playing Roller Derby and joining the Mansfield Roller Derby, you'll want to start by joining one of our Beginner programs.

We accept skaters of all abilities, so even if you've never skated before our dedicated coaching team will take you through all of the skills you will need to play Roller Derby.

All we ask is that you're over the age of 18 - there's no upper age limit and you can be any size or shape. If you don't already have your own skates and protective equipment, we have limited stocks of kit that can be loaned to new skaters.

Become a referee
----------------
If you would like to skate with the league, but don't want to play Roller Derby, you can join us as a Referee. We are always looking for people to join our team of referees. You will learn all of the skills and rules needed to be a referee for our team.

We are always recruiting for referees, get in touch for more information on how to join us!

Become an NSO or Volunteer
--------------------------
You don't have to skate to join Mansfield Roller Derby; there are tons of ways of helping the league by becoming an NSO or Volunteer. We are always recruiting non-skating officials, so please get in touch if you'd like to get involved.

If you are interested in knowing more about joining the league, please get in touch with us. We'll be happy to have you aboard!
